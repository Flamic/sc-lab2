﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public static class StringExtension
    {
        public static string SplitWith(this IEnumerable<string> strings, char c, int count)
        {
            return string.Join("\n" + new string(c, count) + "\n", strings);
        }
    }
}
