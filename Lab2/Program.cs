﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Lab2
{
    class Program
    {
        private const string DASH_SPLITTER = "---------------\n";
        private const string EQUAL_SPLITTER = "===============\n";

        public static void Main(string[] args)
        {
            var ctx = new DeliveryEntities();

            Console.Write("Search by name: ");
            var searchName = Console.ReadLine();
            var count = ctx.Customers.Count(c =>
                c.Name.StartsWith(searchName) || c.Surname.StartsWith(searchName)
            );

            var data = ctx.Customers
                .Where(c => c.Name.StartsWith(searchName) || c.Surname.StartsWith(searchName))
                .OrderBy(c => c.Name)
                .ThenBy(c => c.Surname)
                .Select(c =>
                    " Name: " + c.Name
                    + "\n Surname: " + c.Surname
                    + "\n Phone: +" + c.PhoneNumber
                    + "\n Count of orders: " + c.Orders.Count()
                )
                .ToArray()
                .SplitWith('-', 15);
            Console.WriteLine("{0}Select customers (count: {1}):\n{2}\n{0}\n", EQUAL_SPLITTER, count, data);


            var orders = ctx.Orders.GroupBy(o => o, o => o.OrderItems);
            var str = new StringBuilder();
            foreach (var o in orders)
            {
                str.Append(
                    (o.Key.Courier == null ? " No courier" : " Courier: " + o.Key.Courier.Name + " " + o.Key.Courier.Surname )
                    + "\n Customer: " + o.Key.Customer.Name + " " + o.Key.Customer.Surname 
                    + "\n Address: " + o.Key.Address.City + ", " + o.Key.Address.Street
                    + (o.Key.Address.HouseNumber == null ? "" : ", " + o.Key.Address.HouseNumber)
                    + "\n Delivery cost: " + o.Key.DeliveryCost
                    + "\n Items: "
                    + string.Join(", ", o.SelectMany(g => g).Select(i => i.Product))
                    + "\n" + DASH_SPLITTER
                );
            }
            Console.WriteLine("{0}Select orders:\n{1}{0}\n", EQUAL_SPLITTER, str);

            data = ctx.Couriers
                .Join(ctx.Transports, c => c.TransportID, t => t.TransportID, (c, t) =>
                    new
                    {
                        FullName = c.Name + " " + c.Surname,
                        TransportType = (c.Transport == null ? "-" : c.Transport.TransportType.Type)
                    })
                .Select(c =>
                    " Name: " + c.FullName
                    + "\n Transport: " + c.TransportType
                )
                .SplitWith('-', 15);
            Console.WriteLine("{0}Select couriers:\n{1}\n{0}", EQUAL_SPLITTER, data);

            var couriers = ctx.Couriers.ToArray(); 
            Array.Sort(couriers, new ReverseCourierComparator());
            data = couriers
                .Select(c => c.Name + " " + c.Surname + " " + c.Patronymic)
                .SplitWith('-', 15);
            Console.WriteLine("{0}Select couriers sorted by name in reversed order:\n{1}\n{0}", EQUAL_SPLITTER, data);

            Console.ReadLine();
        }
    }
}