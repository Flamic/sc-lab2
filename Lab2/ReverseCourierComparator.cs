﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class ReverseCourierComparator : IComparer<Courier>
    {
        public int Compare(Courier x, Courier y)
        {
            int cmp;
            if ((cmp = y.Name.CompareTo(x.Name)) != 0
                || (cmp = y.Surname.CompareTo(x.Surname)) != 0
                || (x.Patronymic != null && y.Patronymic != null && (cmp = y.Patronymic.CompareTo(x.Patronymic)) != 0))
                return cmp;
            return 0;
        }
    }
}
